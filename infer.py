import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow.keras import layers, models
from kerastuner import HyperModel
from kerastuner.tuners import RandomSearch
import os
import json
import shutil


def get_split_data(ds, train_split=0.8, test_splt=0.1, val_split=0.1, shuffle=True, shuffle_size=10000):
    ds_size= len(ds)
    if shuffle:
        ds = ds.shuffle(shuffle_size, seed=12)
        train_size = int(train_split * ds_size)
        val_size = int(val_split * ds_size)
        
        train_ds = ds.take(train_size)
        val_ds = ds.skip(train_size).take(val_size)
        test_ds = ds.skip(train_size).skip(val_size)

        return train_ds, val_ds, test_ds
    



class MyHyperModel(HyperModel):
    def __init__(self, input_shape, n_classes):
        self.input_shape = input_shape
        self.n_classes = n_classes
    
    def build(self, hp):
        model = models.Sequential()

        # Add three Conv2D and MaxPooling2D layers
        for i in range(3):
            model.add(layers.Conv2D(
                filters=hp.Int(f'filters_{i}', min_value=32, max_value=128, step=32),
                kernel_size=(3, 3), activation='relu'))
            model.add(layers.MaxPooling2D((2, 2)))
        
        model.add(layers.Flatten())

        # Dense layer with tunable number of units and dropout rate
        model.add(layers.Dense(
            units=hp.Int('units', min_value=64, max_value=256, step=32),
            activation='relu'))
        model.add(layers.Dropout(
            rate=hp.Float('dropout', min_value=0.2, max_value=0.5, step=0.1)))
        model.add(layers.BatchNormalization())

        # Output layer
        model.add(layers.Dense(self.n_classes, activation='softmax'))
        
        # Compile the model
        model.compile(
            optimizer=tf.keras.optimizers.Adam(
                hp.Float('learning_rate', min_value=1e-4, max_value=1e-2, sampling='LOG')),
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy'])
        
        return model
    

# Define a function to read JSON and move image files
def organize_images(json_path, image_folder):
    # Load the JSON annotations
    with open(json_path, 'r') as json_file:
        annotations = json.load(json_file)

    # Loop through the images
    for image_name in os.listdir(image_folder):
        # Construct the full image path
        image_path = os.path.join(image_folder, image_name)
        # Check if the current file is an image and exists in the annotations
        if os.path.isfile(image_path) and image_name in annotations:
            # Extract the driver state from the annotations
            driver_state = annotations[image_name]['driver_state']
            # Create the directory for the driver state if it doesn't exist
            driver_state_dir = os.path.join(image_folder, driver_state)
            if not os.path.exists(driver_state_dir):
                os.makedirs(driver_state_dir)
            # Move the image to the corresponding directory
            shutil.move(image_path, os.path.join(driver_state_dir, image_name))
            print(f"Moved {image_name} to {driver_state_dir}")
        else:
            print(f"Image {image_name} not found in annotations or is not a file.")