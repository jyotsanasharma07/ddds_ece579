# DriverDrowsiness



## Getting started

Driver fatigue has really become an issue these days. Our team created a model that reads face features and predict if driver is drowsy or not.
########################################### Student Names ##################################<br />
' Jyotsana Sharma: jyotsana@umich.edu                                                       '<br />
' Ashutosh Gangrade: agangrad@umich.edu                                                     '<br />
' Akash Gupta: akashag@umich.edu                                                            '<br />
#############################################################################################<br />

## Introduction

In the fast-evolving landscape of au- tomotive safety, the integration of artificial intelli- gence offers a beacon of hope in mitigating one of the most perilous challenges on the road: driver fatigue. Each year, drowsy driving is implicated in an alarm- ing number of accidents, often resulting in severe in- juries or fatalities. Traditional methods to combat this issue have been reactive rather than proactive, typically failing to address the problem until it’s too late.This project aims to harness the power of ML (Machine Learning) to develop a sophisticated Driver Drowsiness Detection System (DDDS). By leverag- ing data from sensors and cameras, the system will analyze a driver’s physical cues to detect signs of fa- tigue. Using a combination of facial recognition al- gorithms, eye tracking, and yawning detection, this system aspires to alert drivers at the earliest signs of drowsiness, thereby significantly reducing the risk of accidents.

Below is the dataset of images being used and the model is going to predict 4 classes:

![Alt text](outputs/dataset.png)

The model predicts the state of the driver and image below is how it displays:

![Alt text](outputs/output_final.png)


## Model Package requirements:

The model uses these packages and the requirements are as follows: <br />
keras==2.15.0 <br />
matplotlib==3.7.0<br />
numpy==1.24.1<br />
pandas==1.5.3<br />
tensorflow==2.15.0<br />

## files

train: Folder with Train images download and processed <br />
my_dir: Hyper-parameter tuning trials info, best model saved, constants.json for image size, batch size and channels info <br />
outputs: Model evaluation outputs <br />
data_processing.ipynb: Kaggle data to be processed and put into train folder <br />
driver-drowsiness-predictor_model.ipynb: Python notebook to run for the model, run 'Model Architecture' cells if you would like to retrain the model, otherwise skip to  'Pre-trained Model run' and continue running till the end.<br />
infer.py: all functions stored in this file are being used in jupyter notebooks
